# RegexParser
Парсер регулярных выражений. 

Доступны символы *,?,+.  

Скобки в регулярном выражении использовать нельзя, т.к. эта фича не доделана.

Работает оно благодаря динамической генерации state machine. 

Допустим дано регулярное выражение "a*b+c?" , тогда state machine будет генерироваться след. образом.(происходит итерация по символам регулярки и на каждом символе state machine меняется):

![1state.png](1state.png)

![state2.png](state2.png)


Заранее имеется список финальных состояний.
0. Генерируется начальное состояние - R. Добавляем его в список финальных состояний.
1. Встречаем символ a в регулярке 
Создается новое состояние и помечается финальным, но в список пока не добавляем. 
Создается переход из предыдущих финальных состояний в новое.
Список финальных состояний очищается и добавляется то самое новое состояние.
2. Встречаем символ *
В финальном состоянии создаем цикл по символу a 
Все состояния, из которых можно перейти в текущее, помечаем финальными
3.Аналогично пункту 1, но уже с символом б
4.Встречаем символ + 
В финальном состоянии создаем цикл по символу б
5. Аналогично пункту 1, но уже с символом с
6. Встречаем символ ?
Все состояния, из которых можно перейти в текущее, помечаем финальными.

Далее полученную state machine передаем в parser где лежит уже строка с обычным текстом, в котором надо найти подстроки. 

На каждом символе просто проверяем есть ли переходы в другие состояния, если есть переходим, если нет, то проверяем не является ли текущее состояние финальным, если не является, но подстрока не подходит.

Так же была решена была решена проблема со случаем, когда из одного состояния есть несколько переходов по одинаковому символу.

Сделать хотел максимально расширяемым, поэтому за каждый отдельный символ(*,?,+) отвечает свой класс, который меняет state machine как ему надо.

Было ещё желание реализовать скобки в регулярном выражении, например:
(абс)*  
(абс(лд)+)*
Но реализация не закончена
